//exporto todos los tipos para asi manejar de manera separa y ordenada 
//las acciones que se dispararan en el reducer

export const types = {
    viajeActive: 'Guarda un viaje activo',
    viajeAdd: 'Agrega un nuevo viaje',
    viajeLoad: 'Carga los viajes',
    viajeUpdate: 'Actualiza un viaje',
    viajeDelete: 'Elimina un viaje',
    viajeClearActive: 'Limpia el Viaje Activo cuando cierra el modal',
    viajeSearch: 'Busca un viaje',


    viajeroActive: 'Guarda un viajero activo',
    viajeroAdd: 'Agrega un nuevo viajero',
    viajeroLoad: 'Carga los viajeros',
    viajeroUpdate: 'Actualiza un viajero',
    viajeroDelete: 'Elimina un viajero',
    viajeroClearActive: 'Limpia el viajero Activo cuando cierra el modal',
    viajeroSearch: 'Busca un viajero',
}