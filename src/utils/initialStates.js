//exportando los estados iniciales que se van a usar en el reducer

export const initialStateViaje = {
    viajes: [],
    activeViaje: null,
}

export const initialStateViajero = {
    viajeros: [],
    activeViajero: null,
}