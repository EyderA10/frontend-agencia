// en este archivo voy a exportar todos los estados iniciales de cada form

//la propiedad field me permite saber que campo tiene errores y poder mostrarlos
export const validation = {
    msg: "",
    field: "",
    validated: true,
}

export const viajero = {
    cedula: "",
    nombre: "",
    fecha_nacimiento: "",
    num_telf: ""
}

export const viaje = {
    codigo_viaje: "",
    numero_plazas: "",
    destino: "",
    lugar_origen: "",
    precio: ""
}

export const viaje_viajero = {
    viaje_id: "",
    viajero_id: ""
}