import React, { useEffect } from 'react'
import { NavBar } from '../ui/NavBar';
import { SearchNav } from '../ui/SearchNav';
import { TableComp } from '../ui/TableComp';
import { viajeHeads } from '../../utils/allHeads';
import { useAgenciaContext } from '../../hooks/useAgenciaContext';
import { ViajeModal } from '../ui/ViajeModal';
import { useHandleData } from '../../hooks/useHandleData';
import { loadViajes } from '../../helpers/viajeActions';

export const Viaje = () => {

  const { openModal, stateViaje, dispatchViaje } = useAgenciaContext()

  const { data, isValidating, mutate } = useHandleData('viajes')
  useEffect(() => {
    if(!isValidating) {
      dispatchViaje(loadViajes(data.viajes))
    }
  }, [data, isValidating])

  return (
    <>
      <div className='h-50 w-100'>
        <NavBar title="Gestion de Viajes" icon="fas fa-plane-departure"/>
          <div className="flex-column mx-auto" style={{width: '80%'}}>
            <SearchNav title="Crear Viaje" mutate={mutate} searchInput="codigo de viaje"/>
            {isValidating != true
            && 
            <TableComp 
              allData={stateViaje.viajes} 
              allHeads={viajeHeads}
            />}
          </div>
          {
            openModal &&
            <ViajeModal />
          }
      </div>
    </>
  )
}