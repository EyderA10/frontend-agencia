import React from 'react'
import { AgenciaProvider } from './context/AgenciaContext'
import { AppRouter } from './routes/AppRouter'

export const App = () => {
  return (
    <>
      <AgenciaProvider>
        <AppRouter />
      </AgenciaProvider>
    </>
  )
}
