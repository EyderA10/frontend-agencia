import React, { useEffect, useMemo } from 'react'
import { memo } from 'react'
import { Button, Form, FormControl } from 'react-bootstrap'
import { useLocation } from 'react-router-dom'
import { useAgenciaContext } from '../../hooks/useAgenciaContext'
import { useForm } from '../../hooks/useForm'
import { searchViajero } from '../../helpers/viajeroActions'
import { useRef } from 'react'
import { searchViaje } from '../../helpers/viajeActions'

export const SearchNav = memo(({title, mutate, searchInput}) => {

  const { startToOpenModal, dispatchViajero, dispatchViaje } = useAgenciaContext()

  const { value, handleInputchange } = useForm({
    search: ""
  })

  const location = useLocation()
  
  const { search } = value

  // manejando el search de manera que se pueda volver a mostrar 
  //todos los datos de la tabla cuando este vacio
  const searchRef = useRef(false) 

  console.log(location.pathname, search)

  useEffect(() => {
    if(searchRef.current === true && search.length === 0) {
      searchRef.current = false
      if(location.pathname === '/viajeros') {
        mutate('viajeros')
      }else {
        mutate('viajes')
      }
    }
  }, [search])
  

  //con esta funcion vamos a manejar el modal
  const handleModal = () => {
    startToOpenModal()
  }

  const onSubmit = (e) => {
    e.preventDefault();
    if(location.pathname === '/viajeros') {
      dispatchViajero(searchViajero(search))
      searchRef.current = true
    }else {
      searchRef.current = true
      dispatchViaje(searchViaje(search))
    }
  }

  return (
    <>
    <div className="d-flex justify-content-between align-items-center">
          <div className="w-50 py-4">
            <Form className="d-flex" onSubmit={onSubmit}>
              <FormControl
                type="search"
                name="search"
                value={search}
                onChange={handleInputchange}
                placeholder={`Busca por ${searchInput}`}
                className="me-2"
                aria-label="Search"
              />
              <Button type="submit" variant="outline-primary">
                Buscar
              </Button>
            </Form>
          </div>
          <div>
            <Button 
            type="button" 
            variant="dark"
            onClick={handleModal}
            >
              {title}
            </Button>
          </div>
        </div>
    </>
  )
})
