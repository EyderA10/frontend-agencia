import { createContext, useReducer, useState } from "react"
import  * as is from "../utils/initialStates"
import { viajeReducer } from "./viajeReducer"
import { viajeroReducer } from "./viajeroReducer"

//se crea un contexto que permita manejar el estado de la aplicacion
//en distintas partes de la aplicacion
export const AgenciaContext = createContext()

export const AgenciaProvider = ({children}) => {
    const [stateViaje, dispatchViaje] = useReducer(viajeReducer, is.initialStateViaje)
    const [stateViajero, dispatchViajero] = useReducer(viajeroReducer, is.initialStateViajero)
    const [openModal, setOpenModal] = useState(false)

    const startToOpenModal = () => {
        setOpenModal(true);
    }

    const startToCloseModal = () => {
        setOpenModal(false);
    }

    return (
        <AgenciaContext.Provider value={{
            stateViaje,
            dispatchViaje,
            stateViajero,
            dispatchViajero,
            openModal,
            startToOpenModal,
            startToCloseModal
        }}>
            {children}
        </AgenciaContext.Provider>
    )

}