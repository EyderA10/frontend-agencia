
const baseUrl = import.meta.env.VITE_API_URL

export const fetchingData = (endpoint = '', data = {}, method = 'GET') => {
    let url = `${baseUrl}/${endpoint}`
    // console.log(url,method)
    
    if(method == 'GET' || method == 'DELETE') {
        return fetch(url ,{
            method
        })
    }else {
        return fetch(url, {
            method,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })

    }
}