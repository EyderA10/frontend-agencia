import { types } from '../types/types'
import { initialStateViaje } from '../utils/initialStates'

//inicializo el estado del reducer para que inicie con un array vacio
//para luego insertarlos a lo largo del proceso de la aplicacion
//luego el activeViaje me permite saber que viaje necesito mostrar al momento de actualizar

export const viajeReducer = (state = initialStateViaje, action) => {
  switch (action.type) {
    case types.viajeAdd:
      return {
        ...state,
        viajes: [action.payload, ...state.viajes],
      }

    case types.viajeActive:
      return {
        ...state,
        activeViaje: {
          id: action.payload.id,
          ...action.payload,
        },
      }

    case types.viajeLoad:
      return {
        ...state,
        viajes: action.payload
      }

    case types.viajeUpdate:
      return {
        ...state,
        viajes: state.viajes.map((viaje) =>
          viaje.id === action.payload.id ? action.payload.viaje : viaje
        ),
      }

    case types.viajeDelete:
      return {
        ...state,
        viajes: state.viajes.filter((viaje) => viaje.id !== action.payload),
        activeViaje: null,
      }

    case types.viajeClearActive:
      return {
        ...state,
        activeViaje: null,
      }

    case types.viajeSearch:
        return {
            ...state,
            viajes: state.viajes.filter(viaje => viaje.codigo_viaje === action.payload)
        }

    default:
      return state
  }
}
