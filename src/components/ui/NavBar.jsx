import React, { memo } from 'react'

export const NavBar = memo(({icon, title}) => {
  return (
    <>
       <div className='d-flex justify-content-between align-items-center h-25 my-3 mx-auto px-2 border-bottom border-4'
        style={{width: '90%'}}>
        <i className={`${icon} fs-2`}></i>
          <h1 className='text-dark fs-2 my-2'>
            {title}
          </h1>
        </div>
    </>
  )
})