import React, { useEffect, useRef } from 'react'
import '../../assets/css/modal.css'
import { Button, Form } from 'react-bootstrap'
import Modal from 'react-modal'
import { useAgenciaContext } from '../../hooks/useAgenciaContext'
import { useForm } from '../../hooks/useForm'
import { customStyles } from '../../utils/modalCss'
import * as allFields from '../../utils/allFields'
import { fetchingData } from '../../helpers/fetchingData'
import { addNewViajero, cleaningActiveViajero, updateViajero } from '../../helpers/viajeroActions'
Modal.setAppElement('#root')

export const ViajeroModal = () => {
  //importo el context
  const { openModal, startToCloseModal, stateViajero, dispatchViajero } = useAgenciaContext()
  //paso los campos del formluario al hook
  const { value, handleInputchange, reset } = useForm(allFields.viajero)
  const { cedula, nombre, fecha_nacimiento, num_telf } = value 
  const { activeViajero } = stateViajero

  const idViajero = useRef("")

  useEffect(() => {
    //esto me permite saber si se esta editando o creando
    if(idViajero.current !== activeViajero?.id && activeViajero) {
      console.log(activeViajero.fecha_nacimiento.date.split(" ")[0]);
      reset({
        cedula: activeViajero.cedula,
        nombre: activeViajero.nombre,
        fecha_nacimiento: activeViajero.fecha_nacimiento.date.split(" ")[0],
        num_telf: activeViajero.num_telf,
      })
      idViajero.current = activeViajero?.id
    }

    return () => {
      reset()
      idViajero.current = ""
    }
  }, [activeViajero?.id])

  const closeModal = () => {
    startToCloseModal()
    //limpiar el viajero con el que se relleno los campos para saber si
    //esta editando o creando
    dispatchViajero(cleaningActiveViajero())
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    let url = ""
    let method = "POST"
    let text = "creado"
    if(activeViajero != null) {
      url = `update-viajero/${activeViajero?.id}`
      method = "PUT"
      text = "actualizado"
    }else {
      url = "create-viajero"
    }
    try {
      const response = await fetchingData(url, {...value}, method)
      const data = await response.json()
      if(data.status) {
        if(activeViajero != null) {
          dispatchViajero(updateViajero(data.viajero.id, data.viajero))
        }else {
          dispatchViajero(addNewViajero(data.viajero));
        }
        Swal.fire({
          position: "top-end",
          icon: "success",
          title: `Se ha ${text} exitosamente!`,
          showConfirmButton: false,
          timer: 1000,
        })
        
        startToCloseModal()
      }else {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: 'Ha ocurrido un error al intentar enviar estos datos'
        })
      }
    } catch (error) {
      console.log(error)
      Swal.fire({
        icon: "error",
        title: "Error",
        text: 'Ha ocurrido un error al intentar enviar estos datos'
      })
    }
    
  }

  return (
    <Modal
      isOpen={openModal}
      onRequestClose={closeModal}
      style={customStyles}
      closeTimeoutMS={200}
      overlayClassName='modal-fondo'
    >
      <h2 className='w-100 text-center pb-3 border-bottom border-3'>Registra los Viajeros</h2>
      <div className='w-100 mx-auto py-3'>
        <Form onSubmit={handleSubmit}>
          <Form.Group className='mb-3' controlId='cedula'>
            <Form.Label>
              Cedula
            </Form.Label>
              <Form.Control
                type='text'
                name='cedula'
                value={cedula}
                onChange={handleInputchange}
                placeholder='Cedula'
              />
              {/* {msg && type == 1 && (
                <Form.Text className='text-danger'>{msg}</Form.Text>
              )} */}
          </Form.Group>
          <Form.Group className='mb-3' controlId='nombre'>
            <Form.Label>
              Nombre
            </Form.Label>
              <Form.Control
                type='text'
                name='nombre'
                value={nombre}
                onChange={handleInputchange}
                placeholder='Nombre'
              />
              {/* {msg && type == 2 && (
                <Form.Text className='text-danger'>{msg}</Form.Text>
              )} */}
          </Form.Group>
          <Form.Group className='mb-3' controlId='fecha_nacimiento'>
            <Form.Label>
              Fecha de Nacimiento
            </Form.Label>
            <Form.Control
                type='date'
                name='fecha_nacimiento'
                value={fecha_nacimiento}
                onChange={handleInputchange}
                placeholder='Fecha de Nacimiento del Viajero'
              />
              {/* {msg && type == 4 && (
                <Form.Text className='text-danger'>{msg}</Form.Text>
              )} */}
          </Form.Group>
          <Form.Group className='mb-3' controlId='num_telf'>
            <Form.Label>
              Numero de Telofono
            </Form.Label>
            <Form.Control
                type='number'
                name='num_telf'
                value={num_telf}
                onChange={handleInputchange}
                placeholder='Numero de telefono'
              />
              {/* {msg && type == 4 && (
                <Form.Text className='text-danger'>{msg}</Form.Text>
              )}
            </Col> */}
          </Form.Group>
          <div className='mx-auto d-flex justify-content-around align-items-center'>
            <Button variant='primary' type='submit'>
              Guardar
            </Button>
            <Button
              variant='dark'
              type='reset'
              onClick={reset}
            >
              Limpiar
            </Button>
          </div>
        </Form>
      </div>
    </Modal>
  )
}
