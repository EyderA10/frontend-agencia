import React, { useEffect, useMemo } from 'react'
import { NavBar } from '../ui/NavBar'
import { TableComp } from '../ui/TableComp'
import { viajeroHeads } from '../../utils/allHeads';
import { SearchNav } from '../ui/SearchNav'
import { useHandleData } from '../../hooks/useHandleData'
import { useAgenciaContext } from '../../hooks/useAgenciaContext';
import { ViajeroModal } from '../ui/ViajeroModal';
import { loadViajeros } from '../../helpers/viajeroActions';

export const Viajero = () => {

  const { openModal, stateViajero, dispatchViajero } = useAgenciaContext()

  const { data, isValidating, mutate } = useHandleData('viajeros')
  
  useEffect(() => {
    if(!isValidating) {
      dispatchViajero(loadViajeros(data.viajeros))
    }
  }, [data, isValidating])

  return (
    <>
      <div className='h-50 w-100'>
        <NavBar title="Gestion de Viajeros" icon="fas fa-backpack"/>
          <div className="flex-column mx-auto overflow-auto" style={{width: '80%'}}>
            <SearchNav title="Crear Viajero" mutate={mutate} searchInput="nombre"/>
            {isValidating != true
            && 
            <TableComp 
              allData={stateViajero.viajeros} 
              allHeads={viajeroHeads}
            />}
          </div>
          {
            openModal &&
            <ViajeroModal />
          }
      </div>
    </>
  )
}