/*separo y export todas las theads que necesita cada 
 *tabla para hacerlo de manera dinamica
 */

export const viajeroHeads = [
    'Cedula',
    'Nombre',
    'Fecha de Nacimiento',
    'Numero de Telefono'
]

export const viajeHeads = [
    'Codigo de Viaje',
    'Numero de Plazas',
    'Destino',
    'Lugar de Origen',
    'Precio',
]

export const viajeAviajeroHeads = [
    'Destino',
    'Lugar de Origen',
    'Nombre del Cliente',
    'Cedula del Cliente',
]