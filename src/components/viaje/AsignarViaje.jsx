import React, { useEffect, useState } from 'react'
import { Button, Form } from 'react-bootstrap'
import { NavBar } from '../ui/NavBar'
import { TableComp } from '../ui/TableComp'
import { viajeAviajeroHeads } from '../../utils/allHeads'
import { useAgenciaContext } from '../../hooks/useAgenciaContext'
import { DetailViajeroModal } from '../ui/DetailViajeroModal'
import { useForm } from '../../hooks/useForm'
import { useHandleData } from '../../hooks/useHandleData'
import { viaje_viajero } from '../../utils/allFields'
import { fetchingData } from '../../helpers/fetchingData'

export const AsignarViaje = () => {
  const { openModal } = useAgenciaContext()
  const [allViajeros, setAllViajeros] = useState([])
  const [allViajes, setAllViajes] = useState([])

  //manejando el estado de la tabla que se va mostrar dinamicamente con o sin que no hayan datos
  // tambien se mostrara la data y se ira agregando dinamicamente los datos.
  const [status, setStatus] = useState(false)

  const { data, isValidating, mutate } = useHandleData('viajeros-assigned')

  const { value, handleInputchange, reset } = useForm(viaje_viajero)

  const { viaje_id, viajero_id } = value

  const handleSubmit = async (e) => {
    e.preventDefault()
    try {
      const response = await fetchingData('assign-viaje', {...value}, 'POST')
      const dataRes = await response.json();
      if(dataRes.status) {
        mutate({...data, viajeros: dataRes.viajeros}, false)
        Swal.fire({
          position: "top-end",
          icon: "success",
          title: `Se ha asignado el viaje con exito!`,
          showConfirmButton: false,
          timer: 1000,
        })
      }else {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: 'Ha ocurrido un error al intentar enviar estos datos'
        })
      } 
      if (data.viajeros.length < 0) {
        setStatus(true)
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: 'Ha ocurrido un error al intentar enviar estos datos'
      })
    }
  }

  const getViajeros = async () => {
    const response = await fetchingData('viajeros')
    const dataRes = await response.json()
    return dataRes?.viajeros
  }

  const getViajes = async () => {
    const response = await fetchingData('viajes')
    const dataRes = await response.json()
    return dataRes?.viajes
  }

  useEffect(() => {
    getViajeros().then(viajeros => setAllViajeros(viajeros))
    getViajes().then(viajes => setAllViajes(viajes))

    return () => {
      setAllViajeros([])
      setAllViajes([])
    }
  }, [])
  
  
  useEffect(() => {
    //que muestre la data sin tener que enviar data por el form
    if(!isValidating) {
      if (data.viajeros.length > 0) {
        setStatus(true)
      }
    }
    return () => {
      setStatus(false)
    }
  }, [status, isValidating])

  return (
    <>
      <div className='h-50 w-100'>
        <NavBar title='Asignar Viaje a un Viajero' icon='fas fa-address-card' />
        <div
          className='d-flex justify-content-around align-items-center pt-3'
          style={{ width: '95%' }}
        >
          <Form
            className='mx-auto d-flex justify-content-around'
            onSubmit={handleSubmit}
          >
            <Form.Group controlId='viaje_id'>
              <Form.Select
                name='viaje_id'
                value={viaje_id}
                onChange={handleInputchange}
                aria-label='Default select example'
                placeholder='Seleccione un Viaje'
              >
                <option>Selecciona un viaje:</option>
                {allViajes.length > 0 &&
                        allViajes.map(({ destino, id }) => (
                          <option key={id} value={id}>
                            {destino}
                          </option>
                        ))}
              </Form.Select>
              {/* {msg && type == 3 && (
                      <Form.Text className='text-danger'>{msg}</Form.Text>
                    )} */}
            </Form.Group>
            <Form.Group className='mx-4' controlId='viajero_id'>
              <Form.Select
                name='viajero_id'
                value={viajero_id}
                onChange={handleInputchange}
                aria-label='Default select example'
                placeholder='Seleccione un Viajero'
              >
                <option>Selecciona un viajero:</option>
                {allViajeros.length > 0 &&
                        allViajeros.map(({ nombre, id }) => (
                          <option key={id} value={id}>
                            {nombre}
                          </option>
                        ))}
              </Form.Select>
              {/* {msg && type == 4 && (
                      <Form.Text className='text-danger'>{msg}</Form.Text>
                    )} */}
            </Form.Group>
            <div className='mx-auto d-flex justify-content-around align-items-center'>
              <Button className='mx-3' variant='primary' type='submit'>
                Guardar
              </Button>
              <Button
                variant='dark'
                type='reset'
                onClick={reset}
              >
                Limpiar
              </Button>
            </div>
          </Form>
        </div>

        {status && (
          <div className='w-75 mx-auto my-5'>
            {!isValidating && (
              <TableComp allData={data.viajeros} allHeads={viajeAviajeroHeads} />
            )}
          </div>
        )}

        {openModal && <DetailViajeroModal />}
      </div>
    </>
  )
}
