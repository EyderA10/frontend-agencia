import React from 'react'
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate
} from 'react-router-dom';
import { SideBar } from '../components/ui/SideBar';
import { AsignarViaje } from '../components/viaje/AsignarViaje';
import { Viaje } from '../components/viaje/Viaje';
import { Viajero } from '../components/viajero/Viajero';

// Definicion de rutas
export const AppRouter = () => {
  return (
      <Router>
        <div className='d-flex'>
          {/* Importando el sidebar */}
          <SideBar/>
          <Routes>
            <Route path="/viajeros" element={<Viajero/>}/>
            <Route path="/viajes" element={<Viaje/>}/>
            <Route path="/asignar-viaje" element={<AsignarViaje/>}/>
            <Route path="*" element={<Navigate to="viajeros"/>}/>
          </Routes>
        </div>
      </Router>
  )
}