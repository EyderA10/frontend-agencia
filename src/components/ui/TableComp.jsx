import React, { memo } from "react";
import { Table } from "react-bootstrap";
import { useLocation } from "react-router-dom";
import { FilesComp } from "./FilesComp";

export const TableComp = memo(({ allData, allHeads }) => {
  //ya que los ids se repiten en la pagina asignar viajeros 
  //el lolcation me permite saber en que pagina estoy y poder cambiar esa logica
  const location = useLocation()
  return (
    <>
      <Table className="table overflow-auto">
        <thead className="thead-dark text-center">
          <tr>
            {
              allHeads.map((head, index) => {
                return (
                  <th key={index} scope="col">{head}</th>
                )})
            }
          </tr>
        </thead>
        <tbody>
          {
            allData.map(data => {
                return <FilesComp
                    key={location.pathname == '/asignar-viaje' ? Math.random(1) : data.id}
                    data={data}
                />
            })
          }
        </tbody>
      </Table>
    </>
  );
});
