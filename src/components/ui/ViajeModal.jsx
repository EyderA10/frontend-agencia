import React, { useEffect, useRef } from 'react'
import '../../assets/css/modal.css'
import { Button, Form } from 'react-bootstrap'
import Modal from 'react-modal'
import { customStyles } from '../../utils/modalCss'
import { useAgenciaContext } from '../../hooks/useAgenciaContext'
import { useForm } from '../../hooks/useForm'
import * as allFields from '../../utils/allFields';
import { addNewViaje, cleaningActiveViaje, updateViaje } from '../../helpers/viajeActions'
import { fetchingData } from '../../helpers/fetchingData'
Modal.setAppElement('#root')

export const ViajeModal = () => {
  // const [validate, setValidate] = useState([allFields.validation])
  //cambiar de manera dinamico la url y el metodo de la peticion ya que puede ser crear o editar

  //importo el context
  const { openModal, startToCloseModal, stateViaje, dispatchViaje } = useAgenciaContext()
  //paso los campos del formluario al hook
  const { value, handleInputchange, reset } = useForm(allFields.viaje)
  const { codigo_viaje, numero_plazas, destino, lugar_origen, precio } = value 
  const { activeViaje } = stateViaje

  const idViaje = useRef("")

  useEffect(() => {
    //esto me permite saber si se esta editando o creando
    if(idViaje.current !== activeViaje?.id && activeViaje) {
      reset({
        codigo_viaje: activeViaje.codigo_viaje,
        numero_plazas: activeViaje.numero_plazas,
        destino: activeViaje.destino,
        lugar_origen: activeViaje.lugar_origen,
        precio: activeViaje.precio,
      })
      idViaje.current = activeViaje?.id
    }

    return () => {
      reset()
      idViaje.current = ""
    }
  }, [activeViaje?.id])

  const closeModal = () => {
    startToCloseModal()
    //limpiar el viajero con el que se relleno los campos para saber si
    //esta editando o creando
    dispatchViaje(cleaningActiveViaje())
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    let url = ""
    let method = "POST"
    let text = "creado"
    if(activeViaje != null) {
      url = `update-viaje/${activeViaje?.id}`
      method = "PUT"
      text = "actualizado"
    }else {
      url = "create-viaje"
    }
    try {
      const response = await fetchingData(url, {...value}, method)
      const data = await response.json()
      if(data.status) {
        if(activeViaje !== null) {
          dispatchViaje(updateViaje(data.viaje.id, data.viaje))
        }else {
          dispatchViaje(addNewViaje(data.viaje));
        }
        Swal.fire({
          position: "top-end",
          icon: "success",
          title: `Se ha ${text} exitosamente!`,
          showConfirmButton: false,
          timer: 1000,
        })
        startToCloseModal()
      }else {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: 'Ha ocurrido un error al intentar enviar estos datos'
        })
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: 'Ha ocurrido un error al intentar enviar estos datos'
      })
    }
    
  }

  return (
    <Modal
      isOpen={openModal}
      onRequestClose={closeModal}
      style={customStyles}
      closeTimeoutMS={200}
      overlayClassName='modal-fondo'
    >
      <h2 className='w-100 text-center pb-3 border-bottom border-3'>Registra los Viajes</h2>
      <div className='w-100 mx-auto py-3'>
        <Form onSubmit={handleSubmit}>
          <Form.Group className='mb-3' controlId='codigo_viaje'>
            <Form.Label>
              Codigo de Viaje
            </Form.Label>
              <Form.Control
                type='text'
                name='codigo_viaje'
                value={codigo_viaje}
                onChange={handleInputchange}
                placeholder='Codigo de Viaje'
              />
              {/* {msg && type == 1 && (
                <Form.Text className='text-danger'>{msg}</Form.Text>
              )} */}
          </Form.Group>
          <Form.Group className='mb-3' controlId='numero_plazas'>
            <Form.Label>
              Numero de Plazas
            </Form.Label>
              <Form.Control
                type='number'
                name='numero_plazas'
                value={numero_plazas}
                onChange={handleInputchange}
                placeholder='Numero de Plazas'
              />
              {/* {msg && type == 2 && (
                <Form.Text className='text-danger'>{msg}</Form.Text>
              )} */}
          </Form.Group>
          <Form.Group className='mb-3' controlId='destino'>
            <Form.Label>
              Destino
            </Form.Label>
            <Form.Control
                type='text'
                name='destino'
                value={destino}
                onChange={handleInputchange}
                placeholder='Destino del Viajero'
              />
              {/* {msg && type == 4 && (
                <Form.Text className='text-danger'>{msg}</Form.Text>
              )} */}
          </Form.Group>
          <Form.Group className='mb-3' controlId='lugar_origen'>
            <Form.Label>
              Lugar de Origen
            </Form.Label>
            <Form.Control
                type='text'
                name='lugar_origen'
                value={lugar_origen}
                onChange={handleInputchange}
                placeholder='Lugar de Origen del viajero'
              />
              {/* {msg && type == 4 && (
                <Form.Text className='text-danger'>{msg}</Form.Text>
              )}
            </Col> */}
          </Form.Group>
          <Form.Group className='mb-3' controlId='precio'>
            <Form.Label>
              Precio
            </Form.Label>
            <Form.Control
                type='number'
                name='precio'
                value={precio}
                onChange={handleInputchange}
                placeholder='Precio del Viaje'
              />
              {/* {msg && type == 4 && (
                <Form.Text className='text-danger'>{msg}</Form.Text>
              )}
            </Col> */}
          </Form.Group>
          <div className='mx-auto d-flex justify-content-around align-items-center'>
            <Button variant='primary' type='submit'>
              Guardar
            </Button>
            <Button
              variant='dark'
              type='reset'
              onClick={reset}
            >
              Limpiar
            </Button>
          </div>
        </Form>
      </div>
    </Modal>
  )
}
