import useSWR from "swr"

//hook que me permite manejar las peticiones usando la libreria useSWR
//el fetcher es el tipo de request que estoy utilizando para enviar datos al server
const fetcher = (...args) => fetch(...args).then(res => res.json()) 

export const useHandleData = (endpoint, param = "") => {
    let url
    let baseURL = import.meta.env.VITE_API_URL
    
    if(param != "") {
        url = `${baseURL}/${endpoint}/${param}`
    }else {
        url = `${baseURL}/${endpoint}`
    }
    const { data, error, isValidating, mutate } = useSWR(url, fetcher, {
        revalidateOnFocus: false,
        revalidateIfStale: false
    })

    return { data, error, mutate, isValidating }

}