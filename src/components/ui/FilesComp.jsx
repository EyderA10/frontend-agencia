import React, { memo } from "react"
import { useLocation } from "react-router-dom";
import { fetchingData } from "../../helpers/fetchingData";
import { activeViaje, deleteViaje } from "../../helpers/viajeActions";
import { activeViajero, deleteViajero } from "../../helpers/viajeroActions";
import { useAgenciaContext } from "../../hooks/useAgenciaContext";

export const FilesComp = memo(({data}) => {

    const { startToOpenModal, dispatchViajero, dispatchViaje } = useAgenciaContext()

    const handleModalViajeroDetail = () => {
      startToOpenModal()
      dispatchViajero(activeViajero(data))
    }

    const location = useLocation()

    const handleModal = () => {
      startToOpenModal()

      //despues de abrir el modal llamo el dispatch para que cargue los datos 
      //del viajero/viaje para posterior actualizarlo en la base de datos

      if(data.cedula) {
        dispatchViajero(activeViajero(data))
      }else if(data.codigo_viaje) {
        dispatchViaje(activeViaje(data))
      }
    }

    const handleRemoveData = async (e) => {
      e.preventDefault();
      //se pasa el endpoint de manera dinamica para poder eliminar viajero/viaje
      let endpoint = ""
      if(data.cedula) {
        endpoint = "delete-viajero"
      }else if(data.codigo_viaje) {
        endpoint = "delete-viaje"
      }
      try {
        Swal.fire({
          title: "¿Estas seguro de eliminarlo?",
          text: "No podras recuperarlo luego!",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Si, eliminar!",
        }).then(async (result) => {
          if (result.isConfirmed) {
            const response = await fetchingData(`${endpoint}/${data.id}`, {}, 'DELETE')
            const resData = await response.json()
            if(resData.status == true) {
              if(data.cedula) {
                dispatchViajero(deleteViajero(data.id))
              }else if(data.codigo_viaje) {
                dispatchViaje(deleteViaje(data.id))
              }
            }else {
              Swal.fire({
                icon: "error",
                title: "Error",
                text: 'Ha ocurrido un error al intentar enviar estos datos'
              })
            }
          }
        })
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: 'Ha ocurrido un error al intentar enviar estos datos'
        })
      }
    }

    if(location.pathname == '/asignar-viaje') {
      return (
        <>
        <tr className="text-center">
          <td scope="row">
          <i style={{cursor: 'pointer'}} 
              className="fas fa-info-square text-primary mx-2"
              onClick={handleModalViajeroDetail}></i>
            {data.destino}
          </td>
          <td>{data.lugar_origen}</td>
          <td>{data.nombre}</td>
          <td>{data.cedula}</td>
        </tr>
      </>
      )
    }

    return (
      <>
        <tr className="text-center">
          <td scope="row">
              {data.numViaje ? 
              (<i style={{cursor: 'pointer'}} 
              className="fas fa-info-square text-primary mx-2"
              onClick={handleModalViajeroDetail}></i>) :
              (<><i style={{cursor: 'pointer'}} 
                  className="far fa-edit text-warning"
                  onClick={handleModal}></i>
              <i
                className="far fa-trash-alt text-danger btn-delete mx-2"
                onClick={handleRemoveData}
                style={{cursor: 'pointer'}}
              ></i></>)}
            {data.cedula ? data.cedula : data.codigo_viaje}
          </td>
          <td>{data.nombre ? data.nombre : data.numero_plazas}</td>
          { data.numViaje && <td>{data.numViaje}</td>}
          <td>{data.fecha_nacimiento?.date ? data.fecha_nacimiento?.date : data.destino}</td>
            <td>{data.num_telf ? data.num_telf : data.lugar_origen}</td>
          {data.precio && <td>{data.precio}</td>}
        </tr>
      </>
  )
})
