import { useState } from "react"

// hook que me permite manejar el estado de los formularios
export const useForm = (initialState = {}) => {

    const [value, setValue] = useState(initialState)

    const handleInputchange = ({target}) => {
        setValue({
            ...value,
            [target.name]: target.value
        })
    }

    const reset = (newFormState = initialState) => {
        setValue(newFormState)
    }

    return {
        value,
        handleInputchange,
        reset
    }

}