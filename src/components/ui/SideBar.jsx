import React from 'react';
import { NavLink } from 'react-router-dom';

export const SideBar = () => {
  return (
    <>
      <div
        className='d-flex flex-column flex-shrink-0 p-3 text-white bg-dark'
        style={{ width: '280px' }}
      >
          <div>
              <i className="far fa-suitcase px-2 fs-4"></i>
              <span className='fs-4'>Agencia de Viajes</span>
          </div>
        <hr />
        <ul className='nav nav-pills flex-column mb-auto'>
          <li className='nav-item'>
            <NavLink
              to='/viajeros'
              className='nav-link text-white'
              aria-current='page'
            >
            <i className="fas fa-backpack px-2"></i>
              Viajeros
            </NavLink>
          </li>
          <li>
            <NavLink to='/viajes' className='nav-link text-white'>
            <i className="fas fa-plane-departure px-2"></i>
              Viajes
            </NavLink>
          </li>
          <li>
            <NavLink to='/asignar-viaje' className='nav-link text-white'>
            <i className="fas fa-address-card px-2"></i>
              Asignar Viajes
            </NavLink>
          </li>
        </ul>
        <hr />
        <p className='fw-bold text-center text-uppercase fs-6'>
            Agencia de Viajes &#169; - 2022
        </p>
      </div>

      <div className='b-example-divider'></div>
    </>
  );
};
