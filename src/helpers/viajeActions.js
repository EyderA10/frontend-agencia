//export las acciones que se van a disparar desde el componente viajero al reducer
import { types } from '../types/types'

export const addNewViaje = (viaje) => ({
    type: types.viajeAdd,
    payload: viaje
})

export const loadViajes = (viajes) => ({
    type: types.viajeLoad,
    payload: viajes
})

export const activeViaje = (viaje) => ({
    type: types.viajeActive,
    payload: {
        id: viaje.id,
        ...viaje
    }
})

export const updateViaje = (id, viaje) => ({
    type: types.viajeUpdate,
    payload: {
        id,
        viaje
    }
})

export const deleteViaje = (id) => ({
    type: types.viajeDelete,
    payload: id
})

export const searchViaje = (nombre) => ({
    type: types.viajeSearch,
    payload: nombre
})

export const cleaningActiveViaje = () => ({
    type: types.viajeClearActive,
})