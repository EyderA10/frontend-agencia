//export las acciones que se van a disparar desde el componente viajero al reducer
import { types } from '../types/types'

export const addNewViajero = (viajero) => ({
    type: types.viajeroAdd,
    payload: viajero
})

export const loadViajeros = (viajeros) => ({
    type: types.viajeroLoad,
    payload: viajeros
})

export const activeViajero = (viajero) => ({
    type: types.viajeroActive,
    payload: {
        id: viajero.id,
        ...viajero
    }
})

export const updateViajero = (id, viajero) => ({
    type: types.viajeroUpdate,
    payload: {
        id,
        viajero
    }
})

export const deleteViajero = (id) => ({
    type: types.viajeroDelete,
    payload: id
})

export const searchViajero = (nombre) => ({
    type: types.viajeroSearch,
    payload: nombre
})

export const cleaningActiveViajero = () => ({
    type: types.viajeroClearActive,
})