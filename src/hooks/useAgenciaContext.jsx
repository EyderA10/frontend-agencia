import { useContext } from "react"
import { AgenciaContext } from "../context/AgenciaContext"

//hook que me permite importar el contexto de la aplicacion
export const useAgenciaContext = () => {
    const { stateViaje,
            dispatchViaje,
            stateViajero,
            dispatchViajero,
            openModal,
            startToOpenModal,
            startToCloseModal } = useContext(AgenciaContext)
        
    return {
        stateViaje,
        dispatchViaje,
        stateViajero,
        dispatchViajero,
        openModal,
        startToOpenModal,
        startToCloseModal
    }
}
