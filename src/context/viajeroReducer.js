import { types } from '../types/types'
import { initialStateViajero } from '../utils/initialStates'

//inicializo el estado del reducer para que inicie con un array vacio
//para luego insertarlos a lo largo del proceso de la aplicacion
//luego el activeViajero me permite saber que viajero necesito mostrar al momento de actualizar
export const viajeroReducer = (state = initialStateViajero, action) => {
  switch (action.type) {
    case types.viajeroAdd:
      return {
        ...state,
        viajeros: [...state.viajeros, action.payload],
      }

    case types.viajeroActive:
      return {
        ...state,
        activeViajero: {
          id: action.payload.id,
          ...action.payload,
        },
      }
    
    case types.viajeroLoad:
      return {
        ...state,
        viajeros: action.payload
      }

    case types.viajeroUpdate:
      return {
        ...state,
        viajeros: state.viajeros.map((viajero) =>
          viajero.id === action.payload.id ? action.payload.viajero : viajero
        ),
      }

    case types.viajeroDelete:
      return {
        ...state,
        viajeros: state.viajeros.filter(
          (viajero) => viajero.id !== action.payload
        ),
        activeViajero: null,
      }

    case types.viajeroClearActive:
      return {
        ...state,
        activeViajero: null,
      }
    
    case types.viajeroSearch:
      return {
        ...state,
        viajeros: state.viajeros.filter((viajero) => viajero.nombre === action.payload)
      }

    default:
      return state
  }
}
