import React from 'react'
import '../../assets/css/modal.css'
import Modal from 'react-modal'
import { customStyles } from '../../utils/modalCss'
import { useAgenciaContext } from '../../hooks/useAgenciaContext'
import { cleaningActiveViajero } from '../../helpers/viajeroActions'
Modal.setAppElement('#root')

export const DetailViajeroModal = () => {
  const { openModal, startToCloseModal, dispatchViajero, stateViajero } = useAgenciaContext()
  const { activeViajero } = stateViajero

  const closeModal = () => {
    startToCloseModal()
    dispatchViajero(cleaningActiveViajero())
  }

  return (
    <Modal
      isOpen={openModal}
      onRequestClose={closeModal}
      style={customStyles}
      closeTimeoutMS={200}
      overlayClassName='modal-fondo'
    >
        <h2 className='w-100 text-center pb-3 border-bottom border-3'>#Viajero {activeViajero.nombre} <i className='fas fa-address-card'></i></h2>
        <div className='w-100 p-4 overflow-auto'>
            <p className='fs-5'><span className='fs-4 fw-bold'>Cedula</span>: {activeViajero.cedula}</p>
            <p className='fs-5'><span className='fs-4 fw-bold'>Fecha de Nacimiento</span>: {activeViajero.fecha_nacimiento}</p>
            <p className='fs-5'><span className='fs-4 fw-bold'>Numero de Telefono</span>: {activeViajero.num_telf}</p>
            {/* <div className='fs-5'><span className='fs-4 fw-bold'>Viajes</span>: { datosViajero.viajes.length > 0 ? <ul>{
            datosViajero.viajes.map(viaje => {
                return (
                    <li>{viaje}</li>
                )
        })}</ul> : (<span> {datosViajero.nombre} aun no se la asignado ningun viaje!</span>)}</div> */}
        </div>
    </Modal>
  )
}
